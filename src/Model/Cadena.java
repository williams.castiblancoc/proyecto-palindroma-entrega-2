/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author sebas
 */
public class Cadena {
    private String texto;

    public Cadena(String texto) {
        this.texto = texto;
    }
    
    public Cadena(){
        
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    
    
    public String palindromo(){
       String textoPalindromo = null;
        ArrayList<String> palabras = palabras(texto);
        
        textoPalindromo = palindromas(palabras,0);
        
        
        return textoPalindromo;
    }
    

    public static ArrayList<String> palabras(String texto){
        
        String[] palabraAux = texto.split(" ");
        List<String> palabras = new ArrayList<>(Arrays.asList(palabraAux));
            
        return (ArrayList<String>) palabras;
            
          
    }

    private String palindromas(ArrayList<String> palabras, int contador) {
        if(contador > palabras.size()-1){
            return " ";
        }else{
            if(esPalindroma(palabras.get(contador))){
                return palabras.get(contador)+ " " + palindromas(palabras,contador+1);
            }else{
                if(esConvertible(palabras.get(contador),false)){
                    return convertida(palabras.get(contador),0) + " hola" + palindromas(palabras,contador+1);
                }else{
                    return palabras.get(contador)+ " " + palindromas(palabras,contador+1);
                }
            }
        }
        
    }

    private boolean esPalindroma(String palabra) {
        String invertida = new StringBuilder(palabra).reverse().toString();
        return invertida.equals(palabra); 
    }

    private boolean esConvertible(String palabra, boolean convertible) {
        ArrayList<Caracter> caracteres = new ArrayList<Caracter>();
        StringBuilder word = new StringBuilder(palabra);
        caracteres = conversor(0,0,word,caracteres);
        if(numeros(caracteres,0)<2){
            return convertible;
        }else{
            return true;
        }
        
    }
    
    private String convertida(String palabra,int contador){
        return palabra;
    }
    /*el metodo conversor me guarda los caracteres que se repiten al menos 2 veces o 1 en su defecto
    el metodo retorna un arraylist de tipo caracter con estos valores para que en un metodo supererior 
    el metodo superior cuenta cuantos caracteres tienen el valor de cantidad en 1 y en caso de que hayan
    mas de 2 la palabra ya no es convertible a palindroma*/ 
    private ArrayList<Caracter> conversor(int x, int y, StringBuilder palabra, ArrayList<Caracter> caracter){
            
        if(palabra == null){
            return caracter;
        }else{
            if(x==y){
                
                Caracter car = new Caracter(palabra.charAt(x),1);
                caracter.add(car);
                palabra = palabra.deleteCharAt(x);
                System.out.println(caracter.get(x).getCaracteres());
                conversor(x,y+1,palabra,caracter);
            }
            if(y > palabra.length()-1){
                
                return conversor(x+1,0,palabra,caracter);
            
            }else{
                
                if(caracter.get(x).getCaracteres() == palabra.charAt(y)){
                    caracter.get(x).setCantidad(caracter.get(x).getCantidad()+1);
                    palabra = palabra.deleteCharAt(y);
                    return conversor(x+1,0,palabra,caracter);
                    
                    
                }else{
                    return conversor(x,y+1,palabra,caracter);
                }
                
            }
        }
    }
    
    private void organizar(int x,int y, String palabra){
        if(x > palabra.length()-1){
            
        }else{
            
        }
    }

    private int numeros(ArrayList<Caracter> caracteres, int x) {
        if(x>caracteres.size()-1){
            return 0;
        }else{
            if(caracteres.get(x).getCantidad()%2 == 0){
                return numeros(caracteres,x+1);
            }else{
                return numeros(caracteres,x+1)+1;
            }
            
        }
    }
    
}
