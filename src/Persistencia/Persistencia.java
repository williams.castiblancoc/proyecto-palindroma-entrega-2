/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Excepciones.ExcepcionesIO;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author sebas
 */
public class Persistencia {
    public static void GuardarArchivo(File file, String contenido) throws IOException {	
        
	FileWriter fw = new FileWriter(file, false);
	BufferedWriter bfw = new BufferedWriter(fw); 
	bfw.write(contenido,0,contenido.length());
	bfw.close();
	fw.close();
    }
    public static ArrayList<String> leerArchivo(String ruta) throws IOException {

	ArrayList<String>  contenido = new ArrayList<String>();
	FileReader fr=new FileReader(ruta);
	BufferedReader bfr=new BufferedReader(fr);
	String linea="";
	while((linea = bfr.readLine())!=null){
            contenido.add(linea);
	}
	bfr.close();
	fr.close();
        return contenido;
    }
}
