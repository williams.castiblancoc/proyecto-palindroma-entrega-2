/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import Model.*;
import Controller.*;
import Excepciones.ExcepcionesIO;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
/**
 * FXML Controller class
 *
 * @author sebas
 */
public class TextoController implements Initializable {
    private String texto;
    @FXML
    private TextArea txtEntrada;
    @FXML
    private TextArea txtSalida;
    @FXML
    private Button btnVerTexto;
    @FXML
    private Button btnGuardarTexto;
    @FXML
    private Button btnConvertirTextoEscrito;
    @FXML
    private Button btnSubirTexto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        texto = "";
    }    


    @FXML
    private void clickVerTexto(ActionEvent event) {
        txtSalida.setText(texto);
    }

    @FXML
    private void clickGuardarTexto(ActionEvent event) throws ExcepcionesIO{
        String ruta = "";
        FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser);
        fileChooser.setTitle("Open Resource File");
        Window stage = null;
        File file = fileChooser.showSaveDialog(stage);

        if(file != null){
            Logica.GuardarTexto(file,texto);
        }else{
            throw new ExcepcionesIO("Error, el archivo no se pudo guardar");
        }
        
        
    }

    @FXML
    private void clickConvertirTextoEscrito(ActionEvent event) {
        
        texto = Logica.Palindroma(txtEntrada.getText().toLowerCase());
 
    }

    @FXML
    private void clickSubirTexto(ActionEvent event) throws ExcepcionesIO {
        String ruta = "";
        FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser);
        fileChooser.setTitle("Open Resource File");
        Window stage = null;
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
            ruta = file.getAbsolutePath();
        
            texto = Logica.CargarTexto(ruta);

            txtEntrada.setText(texto);
        }else{
            throw new ExcepcionesIO("Error, el archivo no se pudo cargar");
        }
        
    }

    private static void configureFileChooser(
        final FileChooser fileChooser) {      
            fileChooser.setTitle("View Texts");
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));                 
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("txt", "*.txt")
            );
    }
    
}
