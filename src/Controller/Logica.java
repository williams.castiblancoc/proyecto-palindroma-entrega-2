/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Hilos.*;
import Model.Caracter;
import Persistencia.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sebas
 */
public class Logica {

    static String Palindroma(String cadena) {
        cadena = cadena
                .replace(",", "")
                .replace(".", "")
                .replace("-", "")
                .replace("+", "")
                .replace(":", "")
                .replace(";", "")
                .replace("_", "")
                .replace("*", "")
                .replace("~", "")
                .replace("/", "")
                .replace("!", "")
                .replace("#", "")
                .replace("$", "")
                .replace("%", "")
                .replace("&", "")
                .replace("(", "")
                .replace(")", "")
                .replace("=", "")
                .replace("?", "")
                .replace("¿", "")
                .replace("¡", "")
                .replace("{", "")
                .replace("}", "")
                .replace("[", "")
                .replace("]", "")
                .replace("@", "")
                .replace("|", "")
                .replace("°", "")
                .replace("0", "")
                .replace("1", "")
                .replace("2", "")
                .replace("3", "")
                .replace("4", "")
                .replace("5", "")
                .replace("6", "")
                .replace("7", "")
                .replace("8", "")
                .replace("9", "")
                .replace("á", "a")
                .replace("é", "e")
                .replace("í", "i")
                .replace("ó", "o")
                .replace("ú", "u")
                .replace("ü", "u");
        
        
        String convertida = "";
        
        ArrayList<String> palabras = palabras(cadena);
        
        convertida = palindromas(palabras,0);
        return convertida;
    }
    
    private static ArrayList<String> palabras(String cadena) {
        String[] palabraAux = cadena.split(" ");
        List<String> palabras = new ArrayList<>(Arrays.asList(palabraAux));
            
        return (ArrayList<String>) palabras;
    }
    
    private static String palindromas(ArrayList<String> palabras, int contador) {
        if(contador > palabras.size()-1){
            return "";
        }else{
            //hilo
            if(palabras.get(contador).length() >3){
                EsPalindroma palindroma = new EsPalindroma(false,palabras.get(contador));
                EsConvertible convertible = new EsConvertible(false,palabras.get(contador));
                Convertida convertida = new Convertida(palabras.get(contador));
                Thread esPalindroma = new Thread(palindroma);
                Thread esConvertible = new Thread(convertible);
                Thread convertir = new Thread(convertida);
                
                esPalindroma.start();
                esConvertible.start();
                convertir.start();
                try {
                    esPalindroma.join();
                    esConvertible.join();
                    convertir.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
                }
 
                if(palindroma.isPalindroma()){
                    return palabras.get(contador)+ " " + palindromas(palabras,contador+1);
                    
                }else{
                    if(convertible.isEsConvertible()){
                        return convertida.getPalabra() + " " + palindromas(palabras,contador+1);
                    }else{
                        return palabras.get(contador)+ " " + palindromas(palabras,contador+1);
                    }
                }
            }else{
                return palabras.get(contador)+ " " + palindromas(palabras,contador+1);
            }
        }
    }

    public static boolean esPalindroma(String palabra) {
        String invertida = new StringBuilder(palabra).reverse().toString();
        return invertida.equals(palabra);
    }

    public static boolean esConvertible(String palabra, boolean convertible) {
        ArrayList<Caracter> caracteres = new ArrayList<Caracter>();
        caracteres = conversor(0,0,palabra,caracteres);
        if(numeros(caracteres,0)<2){
            
            return true;
        }else{
            
            return convertible;
        }       
    }

    public static String convertida(String palabra) {
        ArrayList<Caracter> caracteres = new ArrayList<Caracter>();
        caracteres = conversor(0,0,palabra,caracteres);
        
        return convertir(caracteres);
    }

    private static ArrayList<Caracter> conversor(int x, int y, String palabra, ArrayList<Caracter> caracteres) {
        
        if(x>palabra.length()-1){
            return caracteres;
        }else{
            
            caracteres.add(new Caracter(palabra.charAt(x),1));

            if(seRepite(palabra,palabra.charAt(x),x-1,false)){

                caracteres.remove(y);
                
                return conversor(x+1,y,palabra,caracteres);
                
            }else{
                
                caracteres.get(y).setCantidad(caracteres.get(y).getCantidad()+verificarCaracter(palabra,x+1,palabra.charAt(x)));
                
                return conversor(x+1,y+1,palabra,caracteres);
            }  
            
        }
    }
    
    private static boolean seRepite(String palabra,char caracter,int contador, boolean es){
        
        if(contador<0){
            return es;
        }else{
            if(palabra.charAt(contador)==caracter){
                return true;
            }else{
                return seRepite(palabra,caracter,contador-1,es);
            }
        }
        
    }

    

    private static int verificarCaracter(String palabra, int contador, char letras) {
        if(contador > palabra.length()-1){
            return 0;
        }else{
            if(palabra.charAt(contador) == letras){
                return verificarCaracter(palabra,contador+1,letras)+1;
                
            }else{
                
                return verificarCaracter(palabra,contador+1, letras);
            }
        }
    }
    
    private static int numeros(ArrayList<Caracter> caracteres, int x) {
        if(x>caracteres.size()-1){
            return 0;
        }else{
            if(caracteres.get(x).getCantidad()%2 == 0){
                return numeros(caracteres,x+1);
            }else{
                return numeros(caracteres,x+1)+1;
            }
            
        }
    }

    private static String convertir(ArrayList<Caracter> palabra) {
        String word;
        word = ladoIzquierdo(palabra,0)+centro(palabra,0)+ladoDerecho(palabra,palabra.size()-1);
        return word;
    }
    
    private static String ladoIzquierdo(ArrayList<Caracter> palabra, int contador) {
        if(contador>palabra.size()-1){
            return "";
        }else{
            if(palabra.get(contador).getCantidad()%2==0){
                String caracteresRepetidos;
                caracteresRepetidos = caracteres(0,palabra.get(contador).getCantidad()/2,palabra.get(contador).getCaracteres());
                return caracteresRepetidos+ladoIzquierdo(palabra,contador+1);
            }else{
                return ladoIzquierdo(palabra,contador+1);
            }
        }
    }
    
    private static String centro(ArrayList<Caracter> palabra, int contador) {
        if(contador>palabra.size()-1){
            return "";
        }else{
            if(palabra.get(contador).getCantidad()%2==0){
                return centro(palabra,contador+1);
            }else{
                String caracteresRepetidos;
                caracteresRepetidos = caracteres(0,palabra.get(contador).getCantidad(),palabra.get(contador).getCaracteres());
                return caracteresRepetidos+centro(palabra,contador+1);
            }
        }
    }
    
    private static String ladoDerecho(ArrayList<Caracter> palabra, int contador) {
        if(contador<0){
            return "";
        }else{
            if(palabra.get(contador).getCantidad()%2==0){
                String caracteresRepetidos;
                caracteresRepetidos = caracteres(0,palabra.get(contador).getCantidad()/2,palabra.get(contador).getCaracteres());
                return caracteresRepetidos+ladoDerecho(palabra,contador-1);
            }else{
                return ladoDerecho(palabra,contador-1);
            }
        }
    }
    
    private static String caracteres(int x, int cantidad, char caracteres) {
        if(x>cantidad-1){
            return "";
        }else{
            return caracteres(x+1,cantidad,caracteres)+caracteres;
        }
    }
    
    static String CargarTexto(String ruta){
        String texto = "";
         ArrayList<String> contenido = new ArrayList();
        try {
            contenido = Persistencia.leerArchivo(ruta);
        } catch (IOException ex) {
            Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        texto = contenido.toString();
        
        return texto;
    }
    
    static void GuardarTexto(File file,String contenido){
        try {
            Persistencia.GuardarArchivo(file,contenido);
        } catch (IOException ex) {
            Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
