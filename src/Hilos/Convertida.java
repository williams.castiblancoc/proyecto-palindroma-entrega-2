/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Controller.Logica;

/**
 *
 * @author sebas
 */
public class Convertida implements Runnable{
    private String palabra;

    public Convertida(String palabra) {
        this.palabra = palabra;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }
    
    
    
    @Override
    public void run() {
        palabra = Logica.convertida(palabra);
    }
    
}
