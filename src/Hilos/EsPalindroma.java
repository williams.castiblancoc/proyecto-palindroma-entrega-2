/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Controller.Logica;

/**
 *
 * @author sebas
 */
public class EsPalindroma implements Runnable{
    private boolean palindroma;
    private String palabra;

    public EsPalindroma(boolean palindroma, String palabra) {
        this.palindroma = palindroma;
        this.palabra = palabra;
    }

    public boolean isPalindroma() {
        return palindroma;
    }

    public void setPalindroma(boolean palindroma) {
        this.palindroma = palindroma;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }


    @Override
    public void run() {
        palindroma = Logica.esPalindroma(palabra);
    }
    
}
