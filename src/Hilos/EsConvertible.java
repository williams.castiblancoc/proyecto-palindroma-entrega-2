/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Controller.Logica;

/**
 *
 * @author sebas
 */
public class EsConvertible implements Runnable{
    private boolean esConvertible;
    private String palabra;

    public EsConvertible(boolean esConvertible, String palabra) {
        this.esConvertible = esConvertible;
        this.palabra = palabra;
    }

    public boolean isEsConvertible() {
        return esConvertible;
    }

    public void setEsConvertible(boolean esConvertible) {
        this.esConvertible = esConvertible;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }
    
    
    
    @Override
    public void run() {
        esConvertible = Logica.esConvertible(palabra, false);
    }
    
}
